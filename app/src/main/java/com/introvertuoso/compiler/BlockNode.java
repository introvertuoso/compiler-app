package com.introvertuoso.compiler;

import android.graphics.Color;

public class BlockNode extends AbstractTreeNode {

	@Override
	public String toString()
	{
		return "Block";
	}
	
	@Override
	public Object execute(Context context) {
		for (int i=0; i<children.size(); i++) {
			if (i+1 < children.size()) {
				children.get(i).setNext(children.get(i+1));
				System.out.println(children.get(i).toString());
			}
		}
//		for(AbstractTreeNode n : children) {
////			System.out.println("values " + context.getVars());
////			System.out.println("names "  + context.getVarNames());
//			if (MainActivity.memTrace) {
//				MainActivity.output.append(
//						"Variables in memory: " + context.getVars()+ "\n");
//			}
//			try {
//				n.execute(context);
//			} catch (RuntimeException e) {
//				MainActivity.output.append(e.getMessage() + "\n");
//			}
//		}
		if (children.size() != 0) {
			if (MainActivity.memTrace) {
				MainActivity.output.append(
						"Variables in memory: " + context.getVars()+ "\n");
			}
			try {
				children.get(0).execute(context);
			} catch (RuntimeException e) {
				MainActivity.appendColoredText(MainActivity.output,
						e.getMessage() + "\n",
						Color.RED);
			}
		}
		executeNext(context);
		return null;
	}

	@Override
	public Object convert(Context context) {
		String [] res = new String[children.size()];
		for(int i=0;i<res.length;i++)
			res[i] = (String)children.get(i).convert(context);
		String result = "";
		String delimiter = "\r\n";
		for (String s : res) {
			result += s + delimiter;
		}
		//return String.join("\r\n", res);
		return result;
	}

}
