package com.introvertuoso.compiler;

import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    // TODO MyNewGrammar.main(String[] args) will become MyNewGrammar.main(InputStream stream)
    // TODO FileInputStream line will be commented
    // TODO MyNewGrammar constructor will take stream instead

    // TODO The WriteNode class should append on the output TextView

    // TODO The ReadNode class should take input from the input EditText (basically remade)

    // TODO executeNext was added to AbstractTreeNode

    // TODO BlockNode remade
    // TODO changed all System.out.println()s to either MainActivity.output.append() or Toast
    // TODO appendColoredText whenever it's needed

    public static MainActivity instance = null;

    public static String codeString = "";
    public static String inputString = "";

    public static boolean memTrace = false;

    public static Button run, memTraceButton;
    public static EditText code;
    public static TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        code = findViewById(R.id.code);
        run = findViewById(R.id.run);
        output = findViewById(R.id.output);
        memTraceButton = findViewById(R.id.runWithMemTrace);

        run.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyNewGrammar.functions = new HashMap<String, FunctionDef>();
                MyNewGrammar.funcStack = new Stack<HashMap<String, FunctionDef>>();
                codeString = code.getText().toString();
                output.setText("");
                run();
            }
        });

        memTraceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyNewGrammar.functions = new HashMap<String, FunctionDef>();
                MyNewGrammar.funcStack = new Stack<HashMap<String, FunctionDef>>();
                memTrace = true;
                codeString = code.getText().toString();
                output.setText("");
                run();
                memTrace = false;
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void run() {
        InputStream stream =
                new ByteArrayInputStream(codeString.getBytes());
        try {
            if (!MyNewGrammar.isJj_initialized_once())
                MyNewGrammar.main(stream);
            else {
                MyNewGrammar.ReInit(stream);
                AbstractTreeNode n = MyNewGrammar.start();
                n.execute(new Context());
            }
        } catch (ParseException e1) {
//            e1.printStackTrace();
            MyNewGrammar.ReInit(stream);
            appendColoredText(output, e1.getMessage() + "\n", Color.RED);
        } catch (FileNotFoundException e2) {
//            e2.printStackTrace();
        }
    }

    public static void appendColoredText(TextView tv, String text, int color) {
        int start = tv.getText().length();
        tv.append(text);
        int end = tv.getText().length();

        Spannable spannableText = (Spannable) tv.getText();
        spannableText.setSpan(new ForegroundColorSpan(color), start, end, 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
