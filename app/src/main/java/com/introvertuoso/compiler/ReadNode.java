package com.introvertuoso.compiler;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.widget.EditText;
import android.widget.Toast;

public class ReadNode extends AbstractTreeNode {
    String varName;
    Context context;

    @Override
    public String toString() {
        return "Read " + varName;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    @Override
    public Object execute(Context context) {
        this.context = context;
        if (context.getVars().containsKey(varName)) {

//		System.out.println("Variable will be overridden value and type wise.");
//		System.out.println("(int = ([\"0\"-\"9\"])+");
//		System.out.println(" double = (()+\".\"()*) | (()*\".\"()+)");
//		System.out.println(" String = (~[\"\\n\"])*)");
//		System.out.println(" char = String of length=1");

//        Scanner sc = new Scanner(System.in);
//        String s = sc.nextLine();

            MainActivity.inputString = "";
            showDialogBox();
            executeNext(context);
//            String s = MainActivity.inputString;
//            int dotCounter = 0, digitCounter = 0;
//            for (char c : s.toCharArray()) {
//                if (c == '.') {
//                    dotCounter++;
//                } else if (Character.isDigit(c)) {
//                    digitCounter++;
//                }
//            }
//            if (dotCounter == 1 && digitCounter == s.length() - 1 && s.length() > 1) {
//                context.getVars().put(varName, Double.parseDouble(s));
//                context.getVarNames().add(varName);
//            } else if (digitCounter == s.length() && s.length() > 0) {
//                context.getVars().put(varName, Integer.parseInt(s));
//                context.getVarNames().add(varName);
//            } else {
//                if (s.length() == 1) {
//                    context.getVars().put(varName, Character.valueOf(s.charAt(0)));
//                    context.getVarNames().add(varName);
//                } else {
//                    context.getVars().put(varName, (String) s);
//                    context.getVarNames().add(varName);
//                }
//            }
        } else {
            // TODO maybe make it do?
//            System.out.println("Read cannot define new variables");
            MainActivity.appendColoredText(
                    MainActivity.output,
                    varName + " not found in this scope" + "\n",
                    Color.RED
            );
            executeNext(context);
        }
        return null;
    }

    @Override
    public Object convert(Context context) {
        return "cin >> " + varName + ";";
    }

    private synchronized void showDialogBox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.instance);
        builder.setTitle("Type your input here");
        builder.setMessage(
                "Variable will be overridden value and type wise." + "\n" +
                        "(int = ([\"0\"-\"9\"])+" + "\n" +
                        " double = (()+\".\"()*) | (()*\".\"()+)" + "\n" +
                        " String = (~[\"\\n\"])*)" + "\n" +
                        " char = String of length=1" + "\n");

        final EditText input = new EditText(MainActivity.instance);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().length() > 0) {
                    MainActivity.inputString = input.getText().toString();
                    String s = MainActivity.inputString;
                    if (context.getVars().get(varName).getClass() == String.class) {
                        context.getVars().put(varName, s);
                        context.getVarNames().add(varName);
                    } else if (context.getVars().get(varName).getClass() == Character.class && s.length() == 1) {
                        context.getVars().put(varName, s.charAt(0));
                        context.getVarNames().add(varName);
                    } else {
                        int dotCounter = 0, digitCounter = 0;
                        for (char c : s.toCharArray()) {
                            if (c == '.') {
                                dotCounter++;
                            } else if (Character.isDigit(c)) {
                                digitCounter++;
                            }
                        }
                        if (dotCounter == 1 && digitCounter == s.length() - 1 && s.length() > 1) {
                            context.getVars().put(varName, Double.parseDouble(s));
                            context.getVarNames().add(varName);
                        } else if (digitCounter == s.length() && s.length() > 0) {
                            context.getVars().put(varName, Integer.parseInt(s));
                            context.getVarNames().add(varName);
                        } else {
                            if (s.length() == 1) {
                                context.getVars().put(varName, s.charAt(0));
                                context.getVarNames().add(varName);
                            } else {
                                context.getVars().put(varName, (String) s);
                                context.getVarNames().add(varName);
                            }
                        }
                    }
                    dialog.dismiss();
                } else {
                    Toast.makeText(
                            MainActivity.instance,
                            "Invalid input!",
                            Toast.LENGTH_SHORT).show();
                    showDialogBox();
                }
            }
        });
        /*
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
         */
        builder.show();
    }
}
