package com.introvertuoso.compiler;

import android.graphics.Color;

public class VariableNode extends ExpressionNode {
	
	@Override
	public String toString()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public Object execute(Context context) {
		if (context.getVars().containsKey(name))
			return context.getVars().get(name);
		else {
//			System.out.println(name + " not found in this scope");
			MainActivity.appendColoredText(
					MainActivity.output,
					name + " not found in this scope" + "\n",
					Color.RED
			);
//			context.getVars().put(name , new Object());
//			context.getVarNames().add(name);
//			return context.getVars().get(name);
			return null;
		}
	}

	@Override
	public Object convert(Context context) {
		return name;
	}

}
