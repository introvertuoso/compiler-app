package com.introvertuoso.compiler;

public class WhileNode extends AbstractTreeNode {

	@Override
	public String toString()
	{
		return "While";
	}
	
	@Override
	public Object execute(Context context) {
		context.startScope();

		while((Boolean)children.get(0).execute(context)) {
			children.get(1).execute(context);
		}

		context.endScope();
		executeNext(context);
		return null;
	}

	@Override
	public Object convert(Context context) {
		return "while(" + children.get(0).convert(context)
				+ "){" + children.get(1).convert(context) + "}";
	}

}
